<?php

return [
	'name' => 'ShopTest',
	'db' => [
		'host' => '127.0.0.1',
//		'host' => 'localhost',
		'port' => '3306',
		'database' => 'shop_test',
//		'database' => 'id2717447_shop_test',
		'username' => 'shopuser',
//		'username' => 'id2717447_shopuser',
		'password' => 'shoppass',
//		'password' => 'aYu52RAcy',
		'charset' => 'utf8',
	],
	'log' => '/project/logs/app.log',
	'routes' => '/config/routes.php',
];
