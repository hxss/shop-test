<?php

namespace controllers;

class Cart
{
	/** Add new Product in current Cart */
	public static function addProduct() {
		$product_id = \Request::$params['product_id'];
		$product = \models\Product::find($product_id)[$product_id];
		$count = \Request::$params['product_count'];

		\models\Cart::current()->put($product, $count);

		\views\View::show('/product/catalog', Product::getCatalog(), false);
	}

	/** Delete Product from current Cart */
	public static function delProduct() {
		$productId = \Request::$params['product_id'];

		\models\Cart::current()->products($productId)->delete();

		\views\View::show('/product/catalog', Product::getCatalog(), false);
	}

	/** Update count of Product in current Cart */
	public static function updateProduct() {
		$productId = \Request::$params['product_id'];
		$count = \Request::$params['product_count'];

		if ($count > 0)
			\models\Cart::current()->products($productId)
			                         ->count($count)
			                         ->save();

		echo Product::getCatalog()['summ'];
	}

}
