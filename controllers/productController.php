<?php

namespace controllers;

class Product
{
	/**
	 * Default action that show all Product's
	 * @return void
	 */
	public static function index() {
		\views\View::show('/product/index', [
			'products' => \models\Product::all(),
		]);
	}

	/**
	 * This action show catalog {@see \controllers\Product::getCatalog()}
	 * @return void
	 */
	public static function catalog() {
		\views\View::show('/product/catalog', self::getCatalog());
	}

	/**
	 * This action collect information about Product's in Cart and about free Product's
	 * @return array {
	 *     @var array $inCartPr Product's in current Cart
	 *     @var array $freePr list of free(no added to current Cart) Product's
	 *     @var int $summ total value of Product's in current Cart
	 * }
	 */
	public static function getCatalog() {
		$inCartPrIds = array_keys(\models\Cart::current()->products());
		$inCartPr = \models\Product::find($inCartPrIds);
		$freePr = \models\Product::except($inCartPrIds);

		$summ = 0;
		foreach ($inCartPr as $product)
			$summ += ($product->count = \models\Cart::current()->products($product->id)->count()) * $product->price;

		return [
			'inCartPr' => $inCartPr,
			'freePr' => $freePr,
			'summ' => $summ,
		];
	}

}
