<?php

class ShopPDOException extends PDOException
{
	protected $pdoex;
	protected $queryString;
	protected $params;

	public function __construct(PDOStatement $sql, PDOException $pdoex = null, $params = []) {
		parent::__construct('', 0, $pdoex);

		$this->message = $pdoex->getMessage();
		$this->code = $pdoex->getCode();
		$this->line = $pdoex->getLine();
		$this->pdoex = $pdoex;
		$this->queryString = $sql->queryString;
		$this->params = $params;
	}

	public function __toString() {
		$str = explode("\n", $this->pdoex);
		$str[0] = str_replace('PDOException: ', 'ShopPDOException: ', $str[0]);
		$str[0] .= "\nQuery: {$this->queryString}";
		$str[0] .= "\nParameters: [" . $this->params2string() . "]";
		$str = implode("\n", $str);

		return $str;
	}

	protected function params2string() {
		$str = "";

		if (!empty($this->params)) {
			foreach ($this->params as $k => $v)
				$str .= "\n    [{$k}] => {$v}";

			$str .= "\n";
		}

		return $str;
	}
}
