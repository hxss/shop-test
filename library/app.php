<?php

/**
 * Static app class uses for store globally accessible $config and $db object.
 * This class govern the overall structure and lifecycle of application.
 */
class app
{
	/** @var array $config contain main application settings */
	public static $config;
	/** @var PDO   $db     provide global db connection */
	public static $db;

	/**
	 * Main method of class. Manage the app lifecycle and initialize app components
	 * @return void
	 */
	public static function init() {
		self::$config = include root . '/config/config.php';
		self::$config['session'] = strcasecmp(php_sapi_name(), 'cli') && session_start()
		    ? session_id()
		    : 'cli';

		self::connect();

		views\View::init();
		Request::init();
		Route::init();

//		self::log();
	}

	/**
	 * Connect to DB
	 * @return PDO
	 */
	public static function connect() {
		$db = false;

		$db = new PDO("mysql:host="    . self::$config['db']['host'] . ";"
			              . "port="    . self::$config['db']['port'] . ";"
			              . "dbname="  . self::$config['db']['database'] . ";"
			              . "charset=" . self::$config['db']['charset'],
			self::$config['db']['username'],
			self::$config['db']['password']
		);
		$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

		self::$db = $db;

		return $db;
	}

	public static function catchException(Throwable $ex) {
		$r = new ReflectionClass('Request');
		$rProps = print_r($r->getStaticProperties(), true);
		$rProps = substr_replace($rProps, 'Request', 0, 5);
		self::log("{$rProps}\n{$ex}");
	}

	/**
	 * Write information to app log file
	 * @param  string $string string that would be writed to log
	 * @return void
	 */
	public static function log(string $string = '') {
		if (!$string)
			$string = print_r($_SERVER, true);

		$log = fopen(root . self::$config['log'], 'a');

		self::lwrite($log, (new DateTime)->format('[ Y-m-d H:i:s ]'));
		self::lwrite($log, $string);

		self::lwrite($log);

		fclose($log);
	}

	private static function lwrite($handle, $string = '') {
		fwrite($handle, $string . "\n");
	}

}
