<?php

/**
 * Define root constant
 * @var string root Project root path
 */

define('root', dirname(__DIR__));

include_once __DIR__ . '/functions.php';

/**
 * This class uses for classes autoload
 */
class Autoload
{
	/** @var array  $ignore contain paths that should be ignored at searching */
	private static $ignore = [root . '/public'];

	/** @var bool   $found         this flag uses for stop searching */
	private static $found;
	/** @var string $fullClassName contain full class name with namespace */
	private static $fullClassName;
	/** @var string $className     contain only class name without namespace */
	private static $className;

	/**
	 * Start searching class
	 * @param  string $className class name with or without namespace
	 * @return void
	 */
	public static function start($className) {
		self::$found = false;
		self::$fullClassName = strtolower($className);
		self::$className = self::getShortName(self::$fullClassName);

		$path = root;

		$simplePath = $path . '/' . str_replace('\\', '/', self::$fullClassName) . '.php';

		if (!self::load($simplePath))
			self::find($path);
	}

	/**
	 * This method try to include file and check for searching class was found
	 * @param  string $file path to file
	 * @return bool
	 */
	protected static function load($file) {
		return is_file($file)
		       && strcasecmp(substr($file, -4), '.php') == 0 // ext .php
		       && stripos(implode('', file($file)), "class " . self::$className) // contain declaration of searching class
		       && (include_once $file)
		       && class_exists(self::$fullClassName, false)
		       && self::$found = true;
	}

	/**
	 * Recursive searching of class in the specified path
	 * @param  string $path
	 * @return void
	 */
	protected static function find($path) {
		if (!self::load($path . '/' . self::$className . '.php')) {
			$subdirs = [];
			$dir = opendir($path);

			while (!self::$found
			       && $entry = readdir($dir)) {
				$fullEntry = $path . '/' . $entry;

				if (!in_array($entry, ['.', '..'])
				    && !in_array($fullEntry, self::$ignore)
				    && !self::load($fullEntry)
				    && is_dir($fullEntry))
					$subdirs[] = $fullEntry;
			}

			foreach ($subdirs as $subdir)
				if (!self::$found)
					self::find($subdir);
		}
	}

	/**
	 * @param  string $fullClassName full class name with namespace
	 * @return string class name without namespace
	 */
	protected static function getShortName($fullClassName) {
		$namespace = explode('\\', $fullClassName);

		return array_pop($namespace);
	}

}

spl_autoload_register('Autoload::start');
