<?php

function pre($a) {
	echo "\n<pre>";
	print_r($a);
	echo "</pre>\n";
}

function is_method($callable) {
	return !(($callable instanceof Closure)
	    || (is_string($callable)
	        && function_exists($callable)));
}
