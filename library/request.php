<?php

/**
 * This class uses for store and working with request information
 */
class Request
{
	/** @var string $method name of the current http request method */
	public static $method;
	/** @var string $uri requested application URI */
	public static $uri;
	/** @var array $params given http parameters */
	public static $params;

	/**
	 * Collect all request information and save it in class parameters
	 * @return void
	 */
	public static function init() {
		self::$method = strtolower($_SERVER['REQUEST_METHOD'] ?? 'cli');
		self::$uri = strtolower($_SERVER['REQUEST_URI'] ?? $_SERVER['argv'][1] ?? $_SERVER['argv'][0]);
		self::$params = $_REQUEST;

		if (!self::$params)
			parse_str(file_get_contents("php://input"), self::$params);

		self::$params = array_filter(self::$params, function($k) {
			return substr($k, 0, 1) !== '_';
		}, ARRAY_FILTER_USE_KEY );
	}
}
