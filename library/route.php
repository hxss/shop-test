<?php

/**
 * Class add routing mechanism
 */
class Route
{
	/** @var array    $routes     array of all defined Routes */
	protected static $routes;
	/** @var array    $current    array of all rotes that match current request */
	protected static $current;
	/** @var arrat    $allMethods array of all supported HTTP methods */
	protected static $allMethods = ['get', 'post', 'put', 'patch', 'delete'];

	/** @var string   $name     name of Route */
	public $name;
	/** @var array    $methods  array of supported by this Route HTTP methods */
	public $methods;
	/** @var string   $uri      regex pattern that the request uri must match */
	public $uri;
	/** @var array    $params   array of parameters that found in uri by regex */
	public $params;
	/** @var callback $callable function that should be runned by Route */
	public $callable;

	/**
	 * Construct Route object
	 * @param string|array $methods  {@see Route::$methods}
	 * @param string       $uri      {@see Route::$uri}
	 * @param callback     $callable {@see Route::$callable}
	 * @param string|null  $name     {@see Route::$name}
	 */
	public function __construct($methods, string $uri, callable $callable, $name = false) {
		$methods = (array) $methods;

		if (!self::isHttpMethod($methods))
			throw new InvalidArgumentException('$methods must be a string or array of HTTP methods');

		$this->name = $name ?: count(self::$routes);
		$this->methods = array_intersect(self::$allMethods, $methods);
		$this->uri = $uri;
		$this->callable = is_string($callable) && strpos($callable, '::')
			? explode('::', $callable)
			: $callable;

		self::$routes[$this->name] = $this;
	}

	/**
	 * Initialize class and load defined Routes
	 * @return void
	 */
	public static function init() {
		self::$current = [];
		self::$routes = [];
		include root . app::$config['routes'];
	}

	/**
	 * Run this Route callable function
	 * @return mixed callable function result
	 */
	public function run() {
		$callable = is_method($this->callable)
			? new ReflectionMethod($this->callable[0], $this->callable[1])
			: new ReflectionFunction($this->callable);

		$params = [];
		foreach ($callable->getParameters() as $param)
			if (isset($this->params[$param->name]))
				$params[$param->name] = $this->params[$param->name];

		if (($callable instanceof ReflectionMethod)
			&& !$callable->isStatic()
			&& is_string($this->callable[0]))
			$this->callable[0] = new $this->callable[0];

		return call_user_func_array($this->callable, $params);
	}

	/**
	 * Set name of this Route
	 * @param  string $name new Route name
	 * @return Route        this Route
	 */
	public function name($name) {
		self::$routes[$name] = $this;
		unset(self::$routes[$this->name]);
		$this->name = $name;

		return $this;
	}

	/**
	 * Activate(add to {@see Route::$current} array) this Route
	 * @return Route this Route
	 */
	public function activate() {
		return self::$current[] = $this;
	}

	/**
	 * @return Route first {@see Route::$current} Route
	 */
	public static function current() {
		return self::currents()[0];
	}

	/**
	 * @return array|Route array of {@see Route::$current} Routes
	 */
	public static function currents() {
		if (!self::$current)
			self::findCurrents();

		return self::$current;
	}

	/**
	 * Find Routes that match current Request
	 * @return void
	 */
	private static function findCurrents() {
		foreach (self::$routes as $route)
			if (self::isMatch($route))
				$route->activate();

		if (!self::$current)
			self::$current[] = new static('get', '', function() {});
	}

	/**
	 * @return array|Route all defined Routes
	 */
	public static function all() {
		return self::$routes;
	}

	/**
	 * Add new Route that match specified $methods
	 * @param string|array $methods  {@see Route::$methods}
	 * @param string       $uri      {@see Route::$uri}
	 * @param callback     $callable {@see Route::$callable}
	 * @return Route just added new Route
	 */
	public static function match($methods, $uri, $callable) {
		return new static($methods, $uri, $callable);
	}

	/**
	 * Add new Route that match all HTTP methods
	 * @param string       $uri      {@see Route::$uri}
	 * @param callback     $callable {@see Route::$callable}
	 * @return Route just added new Route
	 */
	public static function any($uri, $callable) {
		$methods = self::$allMethods;
		return new static($methods, $uri, $callable);
	}

	/**
	 * Add new GET Route
	 * @param string       $uri      {@see Route::$uri}
	 * @param callback     $callable {@see Route::$callable}
	 * @return Route just added new Route
	 */
	public static function get($uri, $callable) {
		return new static('get', $uri, $callable);
	}

	/**
	 * Add new POST Route
	 * @param string       $uri      {@see Route::$uri}
	 * @param callback     $callable {@see Route::$callable}
	 * @return Route just added new Route
	 */
	public static function post($uri, $callable) {
		return new static('post', $uri, $callable);
	}

	/**
	 * Add new PUT Route
	 * @param string       $uri      {@see Route::$uri}
	 * @param callback     $callable {@see Route::$callable}
	 * @return Route just added new Route
	 */
	public static function put($uri, $callable) {
		return new static('put', $uri, $callable);
	}

	/**
	 * Add new PATCH Route
	 * @param string       $uri      {@see Route::$uri}
	 * @param callback     $callable {@see Route::$callable}
	 * @return Route just added new Route
	 */
	public static function patch($uri, $callable) {
		return new static('patch', $uri, $callable);
	}

	/**
	 * Add new DELETE Route
	 * @param string       $uri      {@see Route::$uri}
	 * @param callback     $callable {@see Route::$callable}
	 * @return Route just added new Route
	 */
	public static function delete($uri, $callable) {
		return new static('delete', $uri, $callable);
	}

	/**
	 * Check if Route match current Request
	 * @param  Route   $route
	 * @return boolean
	 */
	private static function isMatch(Route $route) {
		if (in_array(Request::$method, $route->methods)
			&& preg_match($route->uri, Request::$uri, $matches))
			return $route->setParams($matches) || true;

		return false;
	}

	/**
	 * Set params for this Route
	 * @param array $matches array of found params
	 * @return array filtered params
	 */
	private function setParams($matches) {
		return $this->params = array_filter($matches, 'is_string', ARRAY_FILTER_USE_KEY);
	}

	/**
	 * Check if granted methods contain HTTP methods
	 * @param  string|array  $methods
	 * @return boolean
	 */
	private static function isHttpMethod($methods) {
		return $methods
			&& array_intersect(self::$allMethods, $methods);
	}

}
