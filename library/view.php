<?php

namespace views;

/**
 * This class provide interface for working with Views and Layouts
 */
class View
{
	/** @var string $layoutDir path to layouts dir */
	protected static $layoutDir;
	/** @var string $tmpl      path to main template file */
	protected static $tmpl;

	/**
	 * Initialize static class
	 * @return void
	 */
	public static function init() {
		self::$layoutDir = root . '/views';
		self::$tmpl = self::$layoutDir . '/main.php';
	}

	/**
	 * Show layout with main template
	 * @param  string  $layout path to layout
	 * @param  array   $vars   variables that would be given to layout
	 * @param  boolean $tmpl   flag for specify using of template
	 * @return void
	 */
	public static function show($layout, $vars = [], $tmpl = true) {
		extract($vars);
		unset($vars);

		include_once $tmpl ? self::$tmpl : self::layout($layout);
	}

	/**
	 * Get the layout full path
	 * @param  string $layoutName name of this layout
	 * @return string
	 */
	public static function layout($layoutName) {
		if (strcasecmp(substr($layoutName, -4), '.php'))
			$layoutName .= '.php';

		return self::$layoutDir . $layoutName;
	}

}
