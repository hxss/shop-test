<?php

namespace models;

use \app;

class Cart extends Model
{
	/** @var string $table {@see Model::$table} */
	public static $table = "cart";
	/** @var Cart $current object of found/created current Cart */
	public static $current;

	/** {@see Model::__construct()} */
	public function __construct($attrs = null) {
		parent::__construct(
			is_null($attrs)
			? $this->current()->attributes
			: $attrs
		);
	}

	/**
	 * Find or create new current Cart
	 * @return Cart
	 */
	public static function current() {
		if (!self::$current
		    && !(self::$current = current(self::where(['user_session' => app::$config['session']])) ?? false)) {
			self::$current = new static(['user_session' => app::$config['session']]);
			self::$current->store();
		}

		return self::$current;
	}

	public function put(Product $product, int $count = 1) {
		$newProduct = new CartProduct([
			'id_product' => $product->id,
			'id_cart' => $this->id,
			'count' => $count
		]);

		return $newProduct->store();
	}

	public function products($idProduct = null) {
		if (!isset($this->id))
			throw new \InvalidArgumentException('Cart id is not set or cart not saved');

		$attrs = ['id_cart' => $this->id, 'id_product' => $idProduct];
		return isset($idProduct)
			? CartProduct::where($attrs)[$idProduct]
			: CartProduct::where($attrs);
	}

//-----------------------------------------------------
// ### MAGIC ###
//-----------------------------------------------------

	public function __get($name) {
		if (strcasecmp($name, 'products') == 0)
			return $this->products;
		else
			return $this->offsetGet($name);
	}
}
