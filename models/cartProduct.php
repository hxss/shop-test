<?php

namespace models;

use \app;

class CartProduct extends Model
{
	/** @var string $table {@see Model::$table} */
	public static $table = "cart_product";
	/** @var string $table {@see Model::$keys} */
	public static $keys = ['id_product', 'id_cart'];

	/** {@see Model::__construct()} */
	public function __construct($attrs = null) {
		if ($attrs['count'] < 1)
			throw new \InvalidArgumentException('CartProduct count must be greater than 0');

		parent::__construct($attrs);
	}

	public function count($newVal = null) {
		if (isset($newVal)) {
			if ($newVal < 1)
				throw new \InvalidArgumentException('CartProduct count must be greater than 0');

			$this->count = $newVal;
			return $this;
		} else
			return $this->count;
	}

	/**
	 * Add product to Cart
	 * @return bool result of query execute
	 */
	public function store() {
		if ($this->exist()) {
			$addCount = $this->count;
			unset($this->count);

			$this->attributes = $this->where($this->attributes)[$this->id_product]->attributes;

			$this->count += $addCount;

			return $this->update();
		} else
			return $this->create();
	}
}
