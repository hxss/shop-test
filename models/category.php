<?php

namespace models;

use \app;
use \PDO;

class Category extends Model
{
	/** @var string $table {@see Model::$table} */
	public static $table = "category";
	/** @var string $tableLink name of table with hierarchical info */
	public static $tableLink = "category_tree";

	/**
	 * Find parent of this Category
	 * @param  int $id id of current Category
	 * @return Category|bool found Category parent or false
	 */
	private static function parent($id) {
		$sql = app::$db->prepare("select c.* "
		    . "from " . self::$table . " c "
		    . "    join " . self::$tableLink . " ct on (ct.id_parent = c.id) "
		    . "where ct.id_child = :id");
		$sql->bindValue(':id', $id, PDO::PARAM_INT);

		try {
			$sql->execute();
		} catch (\PDOException $e) {
			throw new \ShopPDOException($sql, $e, $ids);
		}

		$parent = $sql->fetch();

		return $parent ? self::newInstance([$parent])[$parent['id']] : false;
	}

	private function parent_obj() {
		return self::parent($this->id);
	}

	/**
	 * Find all parents of this Category
	 * @return array|Category flat tree of all found parents and this Category
	 */
	private static function tree($_this) {
		if (!($_this instanceof Category))
			$_this = Category::find($_this)[$_this];

		$tree = [];
		$tree[] = $child = $_this;

		while ($parent = $child->parent_obj())
			$tree[] = $child = $parent;

		return array_reverse($tree);
	}

	private function tree_obj() {
		return self::tree($this);
	}

	public function __call($name, $arguments) {
		$func = [
			'parent' => 'parent_obj',
			'tree' => 'tree_obj',
		];

		return call_user_func([$this, $func[$name]]);
	}

	public static function __callStatic($name, $arguments) {
		$func = [
			'parent' => 'parent',
			'tree' => 'tree',
		];

		return call_user_func_array([self::class, $func[$name]], $arguments);
	}
}
