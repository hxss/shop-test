<?php

namespace models;

use \app;

/**
 * Parent class for all Models
 */
class Model implements \ArrayAccess
{
	/** @var array $attributes attributes of this object */
	protected $attributes;

	/** @var string $table table name for this Model */
	public static $table;
	/** @var array $keys array of primary keys */
	public static $keys = ['id'];

	/**
	 * Construct new object of this class
	 * @param array|null $attrs {@see Model::$attributes}
	 */
	public function __construct($attrs = null) {
		if ($attrs) {
			foreach ($attrs as $offset => $value)
				$this->attributes[$offset] = $value;
		}
	}

	public function __debugInfo() {
		return $this->attributes;
	}

//-----------------------------------------------------
// ### SELECT ###
//-----------------------------------------------------

	/**
	 * Select from DB all items with provided ids
	 * @param  array|int $ids
	 * @return array|Model array of found Models
	 */
	public static function find($ids) {
		return self::select($ids);
	}

	/**
	 * Select from DB all items that has id not in provided ids
	 * @param  array|int $ids
	 * @return array|Model array of found Models
	 */
	public static function except($ids) {
		if ($ids)
			return self::select($ids, 'not in');
		else
			return self::all();
	}

	/**
	 * Select template for MySQL queries
	 * @param  array|int $ids
	 * @param  string|null $cond condition for id (in|not in)
	 * @return array|Model array of found Models
	 */
	private static function select($ids, $cond = 'in') {
		if ($ids) {
			$ids = (array)$ids;

			$in = str_repeat('?,', count($ids) - 1) . '?';

			$sql = app::$db->prepare("select * from " . static::$table . " where " . self::$keys[0] . " {$cond} ({$in})");

			foreach ($ids as $k => $id)
				$sql->bindValue($k + 1, $id, \PDO::PARAM_INT);

			try {
				$sql->execute();
			} catch (\PDOException $e) {
				throw new \ShopPDOException($sql, $e, $ids);
			}

			return self::newInstance($sql->fetchAll());
		} else
			return [];
	}

	/**
	 * Select from DB all items that match provided attributes
	 * @param  array $attrs array of attributes like ['id' => 1, 'name' => 'foo']
	 * @return array|Model array of found Models
	 */
	public static function where($attrs) {
		if (!is_array($attrs))
			return self::find($attrs);

		$conditions = self::attr2cond($attrs);

		$sql = app::$db->prepare("select * from " . static::$table . " where {$conditions}");

		foreach ($attrs as $name => $value)
			if (isset($value))
				$sql->bindValue($name, $value);

		try {
			$sql->execute();
		} catch (\PDOException $e) {
			throw new \ShopPDOException($sql, $e, $attrs);
		}

		return self::newInstance($sql->fetchAll());
	}

	/**
	 * Select from DB all items
	 * @return array|Model array of found Models
	 */
	public static function all() {
		$sql = app::$db->prepare("select * from " . static::$table);

		try {
			$sql->execute();
		} catch (\PDOException $e) {
			throw new \ShopPDOException($sql, $e);
		}

		return self::newInstance($sql->fetchAll());
	}

//-----------------------------------------------------
// ### SAVE ###
//-----------------------------------------------------

	/**
	 * Save object state(update or create DB records)
	 * @return bool result of query execute
	 */
	public function save() {
		if ($this->exist())
			return $this->update();
		else
			return (bool)$this->store();
	}

	/**
	 * Update query template
	 * @return bool result of query execute
	 */
	protected function update() {
		$keys = $this->attr2keys($this->attributes);
		$conditions = $this->attr2cond($keys);

		$attrs = array_diff_key($this->attributes, $keys);
		$values = $this->attr2cond(array_diff_key($this->attributes, $keys), ', ');

		$sql = app::$db->prepare("update " . static::$table . " set {$values} where {$conditions}");

		foreach ($attrs as $name => $value)
			$sql->bindValue($name, $value);

		foreach ($keys as $name => $value)
			$sql->bindValue($name, $value);

		$result = false;
		try {
			$result = $sql->execute();
		} catch (\PDOException $e) {
			throw new \ShopPDOException($sql, $e, [
				'attrs' => $attrs,
				'keys' => $keys,
			]);
		}

		return $result;
	}

	/**
	 * Create in DB record for this object
	 * @return bool result of query execute
	 */
	public function store() {
		$newId = false;
		if ($this->create()) {
			$newId = app::$db->lastInsertId();
			$this->attributes = $this->find($newId)[$newId]->attributes;
		}

		return $newId;
	}

	/**
	 * Create query template
	 * @return bool result of query execute
	 */
	protected function create() {
		$conditions = $this->attr2cond($this->attributes, ', ');

		$sql = app::$db->prepare("insert into " . static::$table . " set {$conditions}");

		foreach ($this->attributes as $name => $value)
			$sql->bindValue($name, $value);

		$result = false;
		try {
			$result = $sql->execute();
		} catch (\PDOException $e) {
			throw new \ShopPDOException($sql, $e, $this->attributes);
		}

		return $result;
	}

	/**
	 * Check if in DB exist record for this object
	 * @return bool result of query execute
	 */
	public function exist() {
		if ($keys = $this->attr2keys($this->attributes)) {
			$conditions = $this->attr2cond($keys);

			$sql = app::$db->prepare("select count(*) from " . static::$table . " where {$conditions}");

			foreach ($keys as $name => $value)
				$sql->bindValue($name, $value);

			try {
				$sql->execute();
			} catch (\PDOException $e) {
				throw new \ShopPDOException($sql, $e, $keys);
			}

			return (bool)$sql->fetchColumn();
		} else
			return false;
	}

//-----------------------------------------------------
// ### DELETE ###
//-----------------------------------------------------

	/**
	 * Delete from DB record for this object
	 * @return bool result of query execute
	 */
	public function delete() {
		if ($this->exist()) {
			$keys = $this->attr2keys($this->attributes);
			$conditions = $this->attr2cond($keys);

			$sql = app::$db->prepare("delete from " . static::$table . " where {$conditions}");

			foreach ($keys as $name => $value)
				$sql->bindValue($name, $value);

			$result = false;
			try {
				$result = $sql->execute();
			} catch (\PDOException $e) {
				throw new \ShopPDOException($sql, $e, $keys);
			}

			return $result;
		} else
			return false;
	}

//-----------------------------------------------------
// ### INSTANCE ###
//-----------------------------------------------------

	/**
	 * Create new object of this class for all items
	 * in array with provided attributes
	 * @param  array $array array of items
	 * @return array|Model array of new objects
	 */
	protected static function newInstance($array) {
		$items = [];
		foreach ($array as $k => $v)
			$items[$v[static::$keys[0]]] = new static($v);

		return $items;
	}

//-----------------------------------------------------
// ### MAGIC ###
//-----------------------------------------------------

	public function __set($name, $value) {
		$this->offsetSet($name, $value);
	}

	public function __get($name) {
		return $this->offsetGet($name);
	}

	public function __isset($name) {
		return $this->offsetExists($name);
	}

	public function __unset($name) {
		$this->offsetUnset($name);
	}

//-----------------------------------------------------
// ### ArrayAccess ###
//-----------------------------------------------------

	public function offsetSet($name, $value) {
		if (is_null($name))
			$this->attributes[] = $value;
		elseif (!in_array($name, static::$keys))
			$this->attributes[$name] = $value;
	}

	public function offsetGet($name) {
		return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
	}

	public function offsetExists($name) {
		return isset($this->attributes[$name]);
	}

	public function offsetUnset($name) {
		unset($this->attributes[$name]);
	}

//-----------------------------------------------------
// ### OTHER ###
//-----------------------------------------------------

	/**
	 * Convert attributes to conditions for MySQL query
	 * @param  array $attrs array of attributes
	 * @param  string|null $glue  separator that would use for implode conditions
	 * @return string
	 */
	public static function attr2cond($attrs, $glue = ' AND ') {
		$conditions = [];
		foreach ($attrs as $name => $value)
			if (isset($value))
				$conditions[] = $name . " = :" . $name;

		return implode($glue, $conditions);
	}

	/**
	 * Convert model keys and attributes values to new array
	 * for selecting from DB by keys
	 * @param  array $attrs array of attributes
	 * @return array of keys and values
	 */
	public function attr2keys($attrs) {
		$keys = array_intersect_key($attrs, array_flip(static::$keys));

		return (count($keys) == count(static::$keys)) ? $keys : false;
	}
}
