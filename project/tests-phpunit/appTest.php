<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers app
 */
final class appTest extends TestCase
{
	public function testInitialize() {
		app::init();

		$this->assertEquals('clia', app::$config['session']);
		$this->assertInstanceOf(PDO::class, app::$db);
	}
}
