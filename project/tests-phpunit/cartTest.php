<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use \models\Cart;
use \models\Product;
use \models\CartProduct;

/**
 * @covers \models\Cart
 * @covers \models\CartProductSet
 */
final class CartTest extends TestCase
{
	protected function setUp() {
		app::init();
	}

//	public function testConstructor() {
//		$cart = new Cart(['user_session' => 'cli_asdasdasd']);
//		$this->assertInstanceOf(Cart::class, $cart);

//		return $cart;
//		$cart = Cart::find(1)[1];

//		pre($cart->products());
//		pre($cart->products(1));

//		var_dump($cart->put(Product::find(1)[1]));
//		pre($cart->products());
//	}

	public function testConstructor() {
		$cart = new Cart(['user_session' => 'cli_asdasdasd']);
		$this->expectException(InvalidArgumentException::class);
		$this->assertNull($cart->products());

		return $cart;
	}

	public function testStore() {
		$cart = new Cart(['user_session' => 'cli_asdasdasd']);
		$this->assertTrue($cart->store() > 0);
		$this->assertEmpty($cart->products());
		$this->assertTrue($cart->delete());
	}

	public function testCurrent() {
		$cart = new Cart();
		$this->assertInstanceOf(Cart::class, $cart);
		$this->assertTrue($cart->exist());
	}

	public function testCurrentStatic() {
		$this->assertInstanceOf(Cart::class, Cart::current());
		$this->assertTrue(Cart::current()->exist());
	}

	public function testProductsAddException() {
		$this->expectException(InvalidArgumentException::class);
		Cart::current()->put(Product::find(1)[1], 0);
	}

	public function testProductsAdd() {
		$this->assertTrue(count(Cart::current()->products()) == 0);

		Cart::current()->put(Product::find(1)[1]);
		$this->assertTrue(count(Cart::current()->products()) == 1);
		$this->assertInstanceOf(CartProduct::class, Cart::current()->products(1));
		$this->assertTrue(Cart::current()->products(1)->exist());

		Cart::current()->put(Product::find(2)[2]);
		$this->assertTrue(count(Cart::current()->products()) == 2);
		$this->assertInstanceOf(CartProduct::class, Cart::current()->products(2));
		$this->assertEquals(2, Cart::current()->products(2)->id_product);
		$this->assertTrue(Cart::current()->products(2)->exist());

		Cart::current()->put(Product::find(3)[3], 333);
		$this->assertTrue(count(Cart::current()->products()) == 3);
		$this->assertInstanceOf(CartProduct::class, Cart::current()->products(3));
		$this->assertEquals(3, Cart::current()->products(3)->id_product);
		$this->assertEquals(333, Cart::current()->products(3)->count());
		$this->assertTrue(Cart::current()->products(3)->exist());
	}

	public function testProductsUpdate() {
		$pr1Count = Cart::current()->products(1)->count();
		Cart::current()->put(Product::find(1)[1]);
		$this->assertEquals($pr1Count + 1, Cart::current()->products(1)->count());

		$pr2Count = Cart::current()->products(2)->count();
		Cart::current()->put(Product::find(2)[2], 123);
		$this->assertEquals($pr2Count + 123, Cart::current()->products(2)->count());

		$cartPr = Cart::current()->products(3)->count(666);
		$this->assertEquals(666, $cartPr->count());
		$cartPr = CartProduct::where([
			'id_cart' => Cart::current()->id,
			'id_product' => 3,
		])[3];
		$this->assertEquals(333, $cartPr->count());

		Cart::current()->products(3)->count(666)->save();
		$cartPr = CartProduct::where([
			'id_cart' => Cart::current()->id,
			'id_product' => 3,
		])[3];
		$this->assertEquals(666, $cartPr->count());
	}

	public function testProductsDelete() {
		foreach (Cart::current()->products() as $cartPr) {
			$this->assertInstanceOf(CartProduct::class, $cartPr);
			$this->assertTrue($cartPr->delete());
		}
	}
}
