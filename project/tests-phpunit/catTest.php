<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use \models\Category;

/**
 * @covers \models\Category
 */
final class CategoryTest extends TestCase
{
	protected function setUp() {
		app::init();
	}

	public function testParent() {
		$parent = Category::find(3)[3]->parent();
		$this->assertInstanceOf(Category::class, $parent);
		$this->assertEquals(2, $parent->id);
	}

	public function testParentStatic() {
		$parent = Category::parent(3);
		$this->assertInstanceOf(Category::class, $parent);
		$this->assertEquals(2, $parent->id);
	}

	public function testTree() {
		$tree = Category::find(3)[3]->tree();
		$this->assertTrue(count($tree) > 0);

		$prev = null;
		foreach ($tree as $cat) {
			$this->assertInstanceOf(Category::class, $cat);

			if ($prev)
				$this->assertEquals($prev, $cat->parent());

			$prev = $cat;
		}
	}

	public function testTreeStatic() {
		$tree = Category::tree(3);
		$this->assertTrue(count($tree) > 0);

		$prev = null;
		foreach ($tree as $cat) {
			$this->assertInstanceOf(Category::class, $cat);

			if ($prev)
				$this->assertEquals($prev, $cat->parent());

			$prev = $cat;
		}
	}
}
