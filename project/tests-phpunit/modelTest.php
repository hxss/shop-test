<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use \models\Product;

/**
 * @covers \models\Product<extended>
 */
final class ProductTest extends TestCase
{
	public $pr;

	protected function setUp() {
		app::init();
		$this->pr = new Product();
	}

// -------------------------------------------------------------
// S E L E C T
// -------------------------------------------------------------

	public function testFind() {
		$product = $this->pr->find(1);
		$this->assertEquals(1, count($product));
		$this->assertInstanceOf(Product::class, $product[1]);
		$this->assertEquals(1, $product[1]->id);

		$product1 = $this->pr->find([1]);
		$this->assertEquals($product[1], $product1[1]);

		$this->assertEmpty($this->pr->find(99));

		$products = $this->pr->find([1, 2]);
		$this->assertEquals(2, count($products));
		$this->assertInstanceOf(Product::class, $products[1]);
		$this->assertEquals(1, $products[1]->id);
		$this->assertInstanceOf(Product::class, $products[2]);
		$this->assertEquals(2, $products[2]->id);
	}

	public function testFindStatic() {
		$product = Product::find(1);
		$this->assertEquals(1, count($product));
		$this->assertInstanceOf(Product::class, $product[1]);
		$this->assertEquals(1, $product[1]->id);

		$product1 = Product::find([1]);
		$this->assertEquals($product[1], $product1[1]);

		$this->assertEmpty(Product::find(99));

		$products = Product::find([1, 2]);
		$this->assertEquals(2, count($products));
		$this->assertInstanceOf(Product::class, $products[1]);
		$this->assertEquals(1, $products[1]->id);
		$this->assertInstanceOf(Product::class, $products[2]);
		$this->assertEquals(2, $products[2]->id);

		return $product;
	}

	public function testExcept() {
		$products = $this->pr->except(1);
		$this->assertTrue(count($products) > 1);
		foreach ($products as $product) {
			$this->assertInstanceOf(Product::class, $product);
			$this->assertFalse($product->id == 1);
		}

		$products1 = $this->pr->except([1]);
		$this->assertEquals($products, $products1);

		$products = $this->pr->except([1, 2, 3, 4]);
		$this->assertEmpty($products);

		$products = $this->pr->except([1, 4]);
		$this->assertEquals(2, count($products));
		$this->assertInstanceOf(Product::class, $products[2]);
		$this->assertEquals(2, $products[2]->id);
		$this->assertInstanceOf(Product::class, $products[3]);
		$this->assertEquals(3, $products[3]->id);

		$products = $this->pr->except(99);
		$this->assertTrue(count($products) > 1);
		foreach ($products as $product)
			$this->assertInstanceOf(Product::class, $product);
	}

	public function testExceptStatic() {
		$products = Product::except(1);
		$this->assertTrue(count($products) > 1);
		foreach ($products as $product) {
			$this->assertInstanceOf(Product::class, $product);
			$this->assertFalse($product->id == 1);
		}

		$products1 = Product::except([1]);
		$this->assertEquals($products, $products1);

		$products = $this->pr->except([1, 2, 3, 4]);
		$this->assertEmpty($products);

		$products = Product::except([1, 4]);
		$this->assertEquals(2, count($products));
		$this->assertInstanceOf(Product::class, $products[2]);
		$this->assertEquals(2, $products[2]->id);
		$this->assertInstanceOf(Product::class, $products[3]);
		$this->assertEquals(3, $products[3]->id);

		$products = Product::except(99);
		$this->assertTrue(count($products) > 1);
		foreach ($products as $product)
			$this->assertInstanceOf(Product::class, $product);

		return $products;
	}

	/**
	 * @depends testExceptStatic
	 */
	public function testAll($products) {
		$this->assertEquals($products, $this->pr->all());
	}

	/**
	 * @depends testExceptStatic
	 */
	public function testAllStatic($products) {
		$this->assertEquals($products, Product::all());
	}

	/**
	 * @depends testFindStatic
	 */
	public function testWhere($product) {
		$this->assertEquals($product, $this->pr->where(1));

		$product1 = $this->pr->where(['price' => 2499]);
		$this->assertEquals($product, $product1);

		$product1 = $this->pr->where(['price' => 2499, 'id_category' => 3]);
		$this->assertEquals($product, $product1);

		$product1 = $this->pr->where(['price' => 2499, 'id_category' => 4]);
		$this->assertEmpty($product1);
	}

	/**
	 * @depends testFindStatic
	 */
	public function testWhereStatic($product) {
		$this->assertEquals($product, Product::where(1));

		$product1 = Product::where(['price' => 2499]);
		$this->assertEquals($product, $product1);

		$product1 = Product::where(['price' => 2499, 'id_category' => 3]);
		$this->assertEquals($product, $product1);

		$product1 = Product::where(['price' => 2499, 'id_category' => 4]);
		$this->assertEmpty($product1);
	}

// -------------------------------------------------------------
// S A V E
// -------------------------------------------------------------

	public function testSave() {
		$pr = new Product([
			'name' => 'testName',
			'manufacturer' => 'testManufacturer',
			'id_category' => 1,
			'img' => 'testImg',
			'price' => 666.99,
			'quantity' => 99,
		]);
		$this->assertInstanceOf(Product::class, $pr);
		$this->assertTrue($pr->save());
		$this->assertTrue($pr->id > 0);
		$this->_testDelete($pr);

		$pr = new Product([
			'id' => 999,
			'name' => 'testName',
			'manufacturer' => 'testManufacturer',
			'id_category' => 1,
			'img' => 'testImg',
			'price' => 666.99,
			'quantity' => 99,
		]);
		$this->assertInstanceOf(Product::class, $pr);
		$this->assertTrue($pr->save());
		$this->assertEquals(999, $pr->id);
		$this->_testDelete($pr);

		$pr = Product::find(1)[1];
		$pr->price = 333;
		$this->assertTrue($pr->save());
		$this->assertEquals(333, Product::find(1)[1]->price);

		$pr->price = 2499;
		$this->assertTrue($pr->save());
		$this->assertEquals(2499, Product::find(1)[1]->price);
	}

// -------------------------------------------------------------
// D E L E T E
// -------------------------------------------------------------

	public function _testDelete($pr) {
		$this->assertTrue($pr->delete());
	}
}
