<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers app
 */
final class requestTest extends TestCase
{
	public function testInitialize() {
		Request::init();

		$this->assertEquals('cli', Request::$method);

		$_SERVER['REQUEST_METHOD'] = 'GET';
		$_SERVER['REQUEST_URI'] = 'test/test2';
		$_REQUEST = [
			'k1' => 'v1',
			'k2' => 'v2',
			'k3' => 'v3',
		];

		Request::init();

		$this->assertEquals('get', Request::$method);
		$this->assertEquals('test/test2', Request::$uri);
		$this->assertEquals([
				'k1' => 'v1',
				'k2' => 'v2',
				'k3' => 'v3',
			], Request::$params);
	}
}
