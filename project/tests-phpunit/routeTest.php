<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers Route
 */
final class RouteTest extends TestCase
{
	protected function setUp() {
		app::$config['routes'] = substr(__DIR__, strlen(root)) . '/zzz.php';
		Route::init();

		Request::$method = 'get';
		Request::$uri = 'tests';
		Request::$params = [];
	}

	public function testCanBeInitialized() {
		$this->assertInternalType('array', Route::all());
	}

	public function testCreateException() {
		$this->expectException(InvalidArgumentException::class);

		Route::match('asd', '', function() {});
	}

	public function testCreating() {
		$oldCount = count(Route::all());

		$uri = '/tests/';
		$callable = function() {};

		$get = Route::get($uri, $callable)->name('my_name');

		Route::any($uri, $callable);
		Route::match(['get', 'asd'], $uri, $callable);

		Route::post($uri, $callable);
		Route::put($uri, $callable);
		Route::patch($uri, $callable);
		Route::delete($uri, $callable);

		$this->assertEquals($oldCount + 7, count(Route::all()));
		$this->assertInstanceOf(Route::class, $get);

		$this->_testCurrents();
		$this->_testCurrent($get);
		$this->_testName($get);
	}

	public function _testCurrents() {
		$this->assertEquals(3, count(Route::currents()));
	}

	public function _testCurrent($get) {
		$this->assertEquals($get, Route::current());
	}

	public function _testName($get) {
		$this->assertEquals($get, Route::all()['my_name']);
	}

	public function testParams() {
		Route::get('/tes(?P<v>.*)/', function($v) {return $v;});

		$this->assertEquals(['v' => 'ts'], Route::current()->params);
		$this->assertEquals('ts', Route::current()->run());
	}

	public function testCallable() {
		$route1 = Route::get('/tests/', function($v = '__empty__') {return $v;});
		$route2 = Route::get('/tests/', 'testFunction');
		$route3 = Route::get('/tests/', ['Test', 'staticMethod']);
		$route4 = Route::get('/tests/', 'Test::staticMethod');
		$test = new Test();
		$route5 = Route::get('/tests/', [$test, 'simpleMethod']);
		$route6 = Route::get('/tests/', ['Test', 'simpleMethod']);
		$route7 = Route::get('/tests/', 'Test::simpleMethod');

		$this->assertEquals('__empty__', $route1->run());
		$this->assertEquals('testFunction', $route2->run());
		$this->assertEquals('Test::staticMethod', $route3->run());
		$this->assertEquals('Test::staticMethod', $route4->run());
		$this->assertEquals('Test::simpleMethod', $route5->run());
		$this->assertEquals('Test::simpleMethod', $route6->run());
		$this->assertEquals('Test::simpleMethod', $route7->run());
	}

	public function testCallableWithParams() {
		$route1 = Route::get('/tes(?P<var>.*)/', function($var = '__empty__') {return $var;});
		$route2 = Route::get('/tes(?P<var>.*)/', 'testFunction');
		$route3 = Route::get('/tes(?P<var>.*)/', ['Test', 'staticMethod']);
		$route4 = Route::get('/tes(?P<var>.*)/', 'Test::staticMethod');
		$test = new Test();
		$route5 = Route::get('/tes(?P<var>.*)/', [$test, 'simpleMethod']);
		$route6 = Route::get('/tes(?P<var>.*)/', ['Test', 'simpleMethod']);
		$route7 = Route::get('/tes(?P<var>.*)/', 'Test::simpleMethod');

		Route::currents();

		$this->assertEquals('ts', $route1->run());
		$this->assertEquals('testFunctionts', $route2->run());
		$this->assertEquals('Test::staticMethodts', $route3->run());
		$this->assertEquals('Test::staticMethodts', $route4->run());
		$this->assertEquals('Test::simpleMethodts', $route5->run());
		$this->assertEquals('Test::simpleMethodts', $route6->run());
		$this->assertEquals('Test::simpleMethodts', $route7->run());
	}
}

function testFunction($var = '') {
	return __FUNCTION__ . $var;
}

class Test
{
	public static function staticMethod($var = '') {
		return __METHOD__ . $var;
	}
	public function simpleMethod($var = '') {
		return __METHOD__ . $var;
	}
}
