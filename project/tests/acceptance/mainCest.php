<?php

class mainCest
{
	public function _before(AcceptanceTester $I) {
//		$I->setCookie('PHPSESSID', 'codeception');
		$I->amOnPage('/');
		$I->setCookie('PHPSESSID', 'codeception');
		$I->amOnPage('/');
		$I->seeInTitle('ShopTest');
	}

	public function viewCatalog(AcceptanceTester $I)
	{
		$I->dontSeeElement('.catalog--cart .product');
		$I->seeElement('.catalog:not(.catalog--cart) .product');
	}

	public function addProductToCart(AcceptanceTester $I) {
		$price1 = (int)$I->grabTextFrom('#product--1 .product__price');
		$I->click('#product--1 .product__action');
// 		$I->waitForElement('.catalog--cart #product--1', 5);
		$I->seeElement('.catalog--cart #product--1');
		$I->dontSeeElement('.catalog:not(.catalog--cart) #product--1');
		$I->seeInField('#product--1 .product__count', '1');
		$I->see($price1, '.catalog--cart .summ .catalog__summ');

		$price3 = (int)$I->grabTextFrom('#product--3 .product__price');
		$count3 = 3;
		$I->fillField('#product--3 .product__count', $count3);
		$I->click('#product--3 .product__action');
		$I->seeElement('.catalog--cart #product--3');
		$I->dontSeeElement('.catalog:not(.catalog--cart) #product--3');
		$I->seeInField('#product--3 .product__count', $count3);
		$I->see($price1 + $price3 * $count3, '.catalog--cart .summ .catalog__summ');
	}

	public function updateProductInCart(AcceptanceTester $I) {
		$I->seeElement('.catalog--cart #product--1');
		$price1 = (int)$I->grabTextFrom('#product--1 .product__price');
		$count1 = $I->grabValueFrom('#product--1 .product__count');

		$I->seeElement('.catalog--cart #product--3');
		$price3 = (int)$I->grabTextFrom('#product--3 .product__price');
		$I->fillField('#product--3 .product__count', random_int(10, 99));
		$count3 = $I->grabValueFrom('#product--3 .product__count');

		$summ = $I->grabTextFrom('.catalog--cart .summ .catalog__summ');
		print_r("c1: {$count1}\n");
		print_r("c1: {$count3}\n");
		print_r("summ: {$summ}\n");

		$I->see($price1 * $count1 + $price3 * $count3, '.catalog--cart .summ .catalog__summ');
	}

	public function deleteProductInCart(AcceptanceTester $I) {
		$I->seeElement('.catalog--cart #product--1');
		$I->click('#product--1 .product__action');
		$I->seeElement('.catalog:not(.catalog--cart) #product--1');
		$I->dontSeeElement('.catalog--cart #product--1');

		$I->seeElement('.catalog--cart #product--3');
		$I->click('#product--3 .product__action');
		$I->seeElement('.catalog:not(.catalog--cart) #product--3');
		$I->dontSeeElement('.catalog--cart #product--3');
	}

}
