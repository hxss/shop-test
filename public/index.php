<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include_once '../library/autoload.php';

set_exception_handler('app::catchException');

app::init();

Route::current()->run();
