
const selectorForm = '.product form';

$(document).ready( function(){

	initForm();

})

function initForm() {
	console.log('initForm');

	$(selectorForm).submit(submitForm)
		.find(':submit[formmethod]')
			.click(formCustomFormmethodClick);

	$('.catalog--cart .product__count').bind('input', updateCount);
}

function submitForm(event) {
	console.log('submitForm');

	event.preventDefault();
	event.stopImmediatePropagation();

	var form = this;

	var method = getFormMethod(form);
	var link = $(form).attr('action');
	var data = $(form).serializeArray();

//	console.log(data);
//	console.log(getFormMethod(form));
//	return;

	$.ajax({
		type: method,
		url: link,
		data: data,
		success: function(response){
//			console.log(response);

			$('#content').html(response);
			initForm();
		}
	});

}

function updateCount(event) {
	console.log('updateCount');
	var form = $(this).closest('form');

	var data = $(form).serializeArray();

	console.log(data);

	$.ajax({
		type: 'POST',
		url: '/cart-product/update',
		data: data,
		success: function(response){
			console.log(response);
			$('.catalog--cart .catalog__summ').html(response);
		}
	});
}

function formCustomFormmethodClick() {
	console.log('formCustomFormmethodClick');

	var form = $(this).closest('form');
	var method = $(this).attr('formmethod');

	setFormMethod(form, method);
}

//#############################################
//### FORM METHOD #############################
//#############################################

function getFormMethod(form) {
	var method = $(form)
		.find('[name=_method]').last()
		.attr('value');

	if (!method)
		method = $(form).attr('method');

	return method;
}

function setFormMethod(form, method) {
	var methodElement = $(form).find('[name=_method]').last()[0];

	if (methodElement) {
		$(methodElement).attr('data-value-default', $(methodElement).attr('value'));
		$(methodElement).attr('value', method);
	}
	else
		$(form).append('<input name="_method" value="' + method + '" type="hidden">');
}

function resetFormMethod(form) {
	var methodElement = $(form).find('[name=_method]').last()[0];

	if (methodElement)
		$(methodElement).attr('value', $(methodElement).attr('data-value-default'));
}
