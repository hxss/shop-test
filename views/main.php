<!DOCTYPE html>
<html lang="">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php echo \app::$config['name'] ?></title>

	<!-- Styles -->
	<link href="/css/styles.css" rel="stylesheet">
</head>
<body>
	<div id="content">
		<?php include_once views\View::layout($layout); ?>
	</div>

	<script src="/js/jquery.min.js"></script>
	<script src="/js/main.js"></script>
</body>
</html>
