<main>
	<section class="catalog catalog--cart">
		<header><h1>Список покупок</h1></header>
		<div class="col-heads">
			<p class="col-head--price">Цена</p>
			<p class="col-head--count">Количество</p>
		</div>
		<div class="products"><?php
				$form = [
					'action' => '/cart-product/delete',
					'method' => 'POST',
					'label' => 'Удалить',
				];
				foreach ($inCartPr as $product)
					include views\View::layout('/product/single');

				unset($form);
		?></div>
		<p class="summ">ИТОГО: <span class="catalog__summ"><?php echo $summ ?></span> руб.</p>
	</section>

	<section class="catalog">
		<header><h1>Список товаров</h1></header>
		<div class="col-heads">
			<p class="col-head--price">Цена</p>
			<p class="col-head--count">Количество</p>
		</div>
		<div class="products"><?php
				foreach ($freePr as $product)
					include views\View::layout('/product/single');
		?></div>
	</section>

</main>
