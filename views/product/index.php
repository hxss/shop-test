<main>
	<section class="catalog">
		<header><h1>Список товаров</h1></header>
		<div class="col-heads">
			<p class="col-head--price">Цена</p>
			<p class="col-head--count">Количество</p>
		</div>
		<?php
			foreach ($products as $product)
				include views\View::layout('/product/single');
		?>
	</section>
</main>
