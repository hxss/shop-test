<?php

$form = $form ?? [
	'action' => '/cart-product',
	'method' => 'POST',
	'label' => 'В корзину',
];

?>
    <article class="product" id="product--<?php echo $product->id ?>">
        <form action="<?php echo $form['action'] ?>" method="POST">
            <input type="hidden" name="product_id" value="<?php echo $product->id ?>">
            <a href="#" class="product__name"><?php echo $product->name ?></a>
            <img src="/img/<?php echo $product->img ?>" alt="" class="product__img">
            <p class="product__price">
                <?php echo $product->price ?> руб.</p>
            <input type="number" class="product__count" name="product_count" value="<?php echo $product->count ?? 1 ?>" min="1">
            <p class="product__manufacturer">
                <?php echo $product->manufacturer ?>
            </p>
            <ul class="product__category">
                <?php foreach (\models\Category::tree($product->id_category) as $cat): ?>
                <li>
                    <?php echo $cat->name ?>
                </li>
                <?php endforeach; ?>
            </ul>
            <input type="submit" class="product__action" value="<?php echo $form['label'] ?>" formmethod="<?php echo $form['method'] ?>">
        </form>
    </article>
